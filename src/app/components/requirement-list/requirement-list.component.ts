import { Component, OnInit } from '@angular/core';
import { Requirement } from 'src/app/models/requirement.model';
import { RequirementService  } from 'src/app/services/requirement.service';

@Component({
  selector: 'app-requirement-list',
  templateUrl: './requirement-list.component.html',
  styleUrls: ['./requirement-list.component.css']
})
export class TutorialsListComponent implements OnInit {

  records: Requirement[] = [];

  constructor(private service: RequirementService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.service.getAll()
      .subscribe({
        next: (data) => {
          this.records = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  refreshList(): void {
    this.getAll();
  }

  deleteItem(id: any): void {
    this.service.delete(id).subscribe({
      next: () => {
        this.refreshList();
      },
      error: (e) => console.error(e)
    })
  }

}