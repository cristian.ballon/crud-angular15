import { Component, Input, OnInit } from '@angular/core';
import { RequirementService } from 'src/app/services/requirement.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Requirement } from 'src/app/models/requirement.model';

@Component({
  selector: 'app-requirement-details',
  templateUrl: './requirement-details.component.html',
  styleUrls: ['./requirement-details.component.css']
})
export class TutorialDetailsComponent implements OnInit {

  @Input() viewMode = false;

  @Input() form: Requirement = {
    id: 0,
    name: '',
    description: '',
    userApplicantId: 0
  };

  message = '';

  constructor(
    private service: RequirementService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getTutorial(this.route.snapshot.params["id"]);
    }
  }

  getTutorial(id: string): void {
    this.service.getById(id)
      .subscribe({
        next: (data) => {
          this.form = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  /*updatePublished(status: boolean): void {
    const data = {
      name: this.currentTutorial.name,
      description: this.currentTutorial.description,
      userApplicantId: this.currentTutorial.userApplicantId,
    };

    this.message = '';

    this.service.update(this.currentTutorial.id, data)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.currentTutorial.published = status;
          this.message = res.message ? res.message : 'The status was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }*/

  updateRecord(): void {
    this.message = '';

    if (!this.form.id) {
      return
    }

    this.service.update(this.form.id, this.form)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'This tutorial was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }

  deleteTutorial(): void {
    this.service.delete(this.form.id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/tutorials']);
        },
        error: (e) => console.error(e)
      });
  }

}