import { Component } from '@angular/core';
import { Requirement } from 'src/app/models/requirement.model';
import { RequirementService } from 'src/app/services/requirement.service';

@Component({
  selector: 'app-add-requirement',
  templateUrl: './add-requirement.component.html',
  styleUrls: ['./add-requirement.component.css']
})
export class AddTutorialComponent {

  selectedFile: File | null = null;

  form: Requirement = {
    name: '',
    description: '',
    userApplicantId: 0
  }
  submitted = false;

  constructor(private service: RequirementService) {
    this.initForm();
  }

  initForm(): void {
    this.form = {
      name: '',
      description: '',
      userApplicantId: 0
    };

    this.selectedFile = null;
  }

  saveRecord(): void {
    if (this.selectedFile === null) {
      console.log('No file selected');
      alert('No file selected');
      return;
    }

    const data = {
      name: this.form.name,
      description: this.form.description,
      userApplicantId: this.form.userApplicantId
    };

    const formData = new FormData();
    formData.append('requirement', JSON.stringify(data));
    if (this.selectedFile) {
      formData.append('file', this.selectedFile, this.selectedFile.name);
    }

    this.service.save(formData)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.submitted = true;
        },
        error: (e) => console.error(e)
      });
  }

  newRecord(): void {
    this.submitted = false;
    this.initForm();
  }

  onFileSelected(event: any) {
    this.selectedFile = <File>event.target.files[0];
  }

}