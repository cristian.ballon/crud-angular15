import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddTutorialComponent } from './components/add-requirement/add-requirement.component';
import { TutorialDetailsComponent } from './components/requirement-details/requirement-details.component';
import { TutorialsListComponent } from './components/requirement-list/requirement-list.component';
import { AyudaComponent } from './ayuda/ayuda.component';
import { RequirementDetailComponent } from './requirement-detail/requirement-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    AddTutorialComponent,
    TutorialDetailsComponent,
    TutorialsListComponent,
    AyudaComponent,
    RequirementDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
