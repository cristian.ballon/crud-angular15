// ayuda.component.ts
import { Component } from '@angular/core';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.css']
})
export class AyudaComponent {
  selectedRequirementType: string = '';

  constructor() { }

  onRequirementChange(): void {
    // Opcional: Implementa lógica adicional si es necesario cuando el tipo cambia
    console.log(this.selectedRequirementType);
  }
}
