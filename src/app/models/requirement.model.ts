
export interface Requirement {
    id?: number;
    name: string;
    description: string;
    fileUrl?: string;
    userApplicantId?: number;
  }