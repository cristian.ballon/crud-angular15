import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Requirement } from '../models/requirement.model';

const baseUrl = `${environment.apiUrl}/${'requirements'}`

@Injectable({
  providedIn: 'root'
})
export class RequirementService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Requirement[]> {
    return this.http.get<Requirement[]>(baseUrl);
  }

  getById(id: any): Observable<Requirement> {
    return this.http.get<Requirement>(`${baseUrl}/${id}`);
  }

  save(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  
}