// requirement-detail.component.ts
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-requirement-detail',
  templateUrl: './requirement-detail.component.html',
  styleUrls: ['./requirement-detail.component.css']
})
export class RequirementDetailComponent implements OnChanges {
  @Input() requirementType: string = '';
  description: string = '';

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['requirementType']) {
      this.updateDescription();
    }
  }

  updateDescription(): void {
    switch(this.requirementType) {
      case '1':
        this.description = 'Este tipo de requerimiento dura más de 100 horas.';
        break;
      case '2':
        this.description = 'Este tipo de requerimiento dura de 5 a 99 horas.';
        break;
      case '3':
        this.description = 'Este tipo de Requerimiento duras a los más 4 horas.';
        break;
      default:
        this.description = 'Seleccione un tipo de requerimiento.';
    }
  }
}
